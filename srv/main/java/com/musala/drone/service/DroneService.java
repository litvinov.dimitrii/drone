package com.musala.drone.service;

import com.musala.drone.controller.dto.DroneDto;
import com.musala.drone.controller.dto.MedicationDto;
import com.musala.drone.entity.Drone;
import com.musala.drone.entity.Medication;
import com.musala.drone.entity.enums.DroneState;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.repository.MedicationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class DroneService {
    private final DroneRepository droneRepository;
    private final MedicationRepository medicationRepository;

    private static final Set<DroneState> READY_TO_LOAD_STATES = Set.of(DroneState.IDLE, DroneState.LOADING);

    public DroneDto create(DroneDto droneDto) {
        Drone drone = convert(droneDto);
        return convertToDto(droneRepository.save(drone));
    }

    public int load(Long droneId, String name, Integer weight, String code, byte[] image) {
        Drone drone = getById(droneId);
        int availableLoadWeight = availableLoadWeight(drone, weight);
        setLoadingState(drone);

        Medication medication = new Medication();
        medication.setName(name);
        medication.setWeight(weight);
        medication.setCode(code);
        medication.setDrone(drone);
        medication.setImage(image);
        medicationRepository.save(medication);

        return availableLoadWeight;
    }

    public List<MedicationDto> getLoad(Long droneId) {
        return getById(droneId).getMedications().stream()
                .map(this::convertToDto)
                .toList();
    }

    public List<DroneDto> getReadyToLoad() {
        return droneRepository.findByBatteryGreaterThanEqualAndStateIn(25, READY_TO_LOAD_STATES).stream()
                .map(this::convertToDto)
                .toList();
    }

    public int batteryLevel(Long droneId) {
        return getById(droneId).getBattery();
    }

    private void setLoadingState(Drone drone) {
        DroneState state = drone.getState();
        if (state == DroneState.LOADING) {
            return;
        }
        if (state == DroneState.IDLE) {
            if (drone.getBattery() < 25) {
                throw new IllegalStateException("Low battery");
            }
            drone.setState(DroneState.LOADING);
            return;
        }
        throw new IllegalStateException("Wrong drone state");
    }

    private int availableLoadWeight(Drone drone, Integer addWeight) {
        int weightLimit = drone.getWeightLimit();
        int currentLoad = drone.getMedications().stream()
                .map(Medication::getWeight)
                .reduce(Integer::sum)
                .orElse(0);
        int availableLoadWeight = weightLimit - currentLoad - addWeight;
        if (availableLoadWeight < 0) {
            throw new IllegalStateException("Overloaded");
        }
        return availableLoadWeight;
    }

    public Drone getById(Long id) {
        return droneRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found"));
    }

    private Drone convert(DroneDto droneDto) {
        Drone drone = new Drone();
        drone.setSerialNumber(droneDto.serialNumber());
        drone.setWeightLimit(droneDto.weightLimit());
        drone.setModel(droneDto.model());
        drone.setBattery(droneDto.battery());
        return drone;
    }

    private DroneDto convertToDto(Drone drone) {
        return new DroneDto(
                drone.getId(),
                drone.getSerialNumber(),
                drone.getWeightLimit(),
                drone.getModel(),
                drone.getBattery(),
                drone.getState()
        );
    }

    private MedicationDto convertToDto(Medication medication) {
        return new MedicationDto(
                medication.getId(),
                medication.getName(),
                medication.getWeight(),
                medication.getCode()
        );
    }
}
