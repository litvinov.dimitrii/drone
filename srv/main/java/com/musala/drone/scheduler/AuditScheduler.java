package com.musala.drone.scheduler;

import com.musala.drone.repository.DroneAuditRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class AuditScheduler {

    private final DroneAuditRepository droneAuditRepository;

    @Transactional
    @Scheduled(fixedDelay = 60000, initialDelay = 10000)
    public void auditBattery() {
        droneAuditRepository.auditDroneBattery();
    }
}
