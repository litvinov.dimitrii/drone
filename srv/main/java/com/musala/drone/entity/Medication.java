package com.musala.drone.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private int weight;

    private String code;

    @Lob
    byte[] image;

    @ManyToOne
    @JoinColumn(name = "drone_id")
    private Drone drone;
}

