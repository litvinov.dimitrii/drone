package com.musala.drone.entity;

import com.musala.drone.entity.enums.DroneModel;
import com.musala.drone.entity.enums.DroneState;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Drone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String serialNumber;

    private int weightLimit;

    @Enumerated(EnumType.STRING)
    private DroneModel model;

    private int battery;

    @Enumerated(EnumType.STRING)
    private DroneState state = DroneState.IDLE;

    @OneToMany(mappedBy = "drone")
    private List<Medication> medications;
}
