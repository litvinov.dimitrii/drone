package com.musala.drone.entity.enums;

public enum DroneModel {
    LIGHTWEIGHT,
    MIDDLEWEIGHT,
    CRUISER_WEIGHT,
    HEAVYWEIGHT
}
