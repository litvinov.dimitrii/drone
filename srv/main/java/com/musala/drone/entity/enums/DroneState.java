package com.musala.drone.entity.enums;

public enum DroneState {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
