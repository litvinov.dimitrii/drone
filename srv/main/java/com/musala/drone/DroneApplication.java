package com.musala.drone;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import static org.springframework.boot.SpringApplication.run;

@EnableScheduling
@SpringBootApplication
public class DroneApplication {
    public static void main(String[] args) {
        run(DroneApplication.class, args);
    }
}
