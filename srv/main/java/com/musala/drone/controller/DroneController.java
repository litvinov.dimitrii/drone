package com.musala.drone.controller;

import com.musala.drone.controller.dto.DroneDto;
import com.musala.drone.controller.dto.MedicationDto;
import com.musala.drone.service.DroneService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.List;

@Validated
@RestController
@RequestMapping("/drone")
@AllArgsConstructor
public class DroneController {
    private final DroneService droneService;

    @PostMapping
    public DroneDto register(@RequestBody @Valid DroneDto droneDto) {
        return droneService.create(droneDto);
    }

    @PostMapping("/load/{droneId}")
    public Integer load(@PathVariable Long droneId,
                        @Pattern(regexp = "^[a-zA-Z0-9-_]*$")
                        @RequestParam String name,
                        @RequestParam Integer weight,
                        @Pattern(regexp = "^[A-Z0-9_]*$")
                        @RequestParam String code,
                        @RequestParam MultipartFile image) throws IOException {
        return droneService.load(droneId, name, weight, code, image.getBytes());
    }

    @GetMapping("/{droneId}/medications")
    public List<MedicationDto> getLoad(@PathVariable Long droneId) {
        return droneService.getLoad(droneId);
    }

    @GetMapping("/ready-to-load")
    public List<DroneDto> getReadyToLoad() {
        return droneService.getReadyToLoad();
    }

    @GetMapping("/{droneId}/check-battery")
    public int checkBattery(@PathVariable Long droneId) {
        return droneService.batteryLevel(droneId);
    }
}
