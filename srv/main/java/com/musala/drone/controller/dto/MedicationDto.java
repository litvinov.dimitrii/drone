package com.musala.drone.controller.dto;

public record MedicationDto(
        Long id,
        String name,
        int weight,
        String code
) {
}
