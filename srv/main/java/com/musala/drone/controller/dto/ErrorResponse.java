package com.musala.drone.controller.dto;

public record ErrorResponse(
        String message
){}
