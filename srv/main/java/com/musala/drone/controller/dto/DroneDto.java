package com.musala.drone.controller.dto;

import com.musala.drone.entity.enums.DroneModel;
import com.musala.drone.entity.enums.DroneState;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@Validated
public record DroneDto(
        Long id,
        @Size(max = 100)
        String serialNumber,
        @Max(500)
        int weightLimit,
        DroneModel model,
        @Max(100)
        int battery,
        DroneState state
) {
}
