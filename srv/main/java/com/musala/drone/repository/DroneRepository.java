package com.musala.drone.repository;

import com.musala.drone.entity.Drone;
import com.musala.drone.entity.enums.DroneState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface DroneRepository extends JpaRepository<Drone, Long> {

    List<Drone> findByBatteryGreaterThanEqualAndStateIn(int battery, Set<DroneState> states);
}
