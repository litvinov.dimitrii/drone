package com.musala.drone.repository;

import com.musala.drone.entity.DroneAudit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DroneAuditRepository extends JpaRepository<DroneAudit, Long> {

    @Modifying
    @Query(value = """
                INSERT INTO drone_audit (drone_id, battery)
                SELECT id, battery
                FROM drone;
            """, nativeQuery = true)
    void auditDroneBattery();
}
