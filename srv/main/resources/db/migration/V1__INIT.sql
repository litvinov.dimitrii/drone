CREATE TABLE IF NOT EXISTS drone
(
    id            INTEGER PRIMARY KEY AUTO_INCREMENT,
    serial_number VARCHAR(100) NOT NULL UNIQUE,
    weight_limit  INT          NOT NULL,
    model         VARCHAR(14)  NOT NULL,
    battery       NUMERIC(3)   NOT NULL,
    state         VARCHAR(10)  NOT NULL
);

CREATE TABLE IF NOT EXISTS drone_audit
(
    id       INTEGER PRIMARY KEY AUTO_INCREMENT,
    drone_id BIGINT     NOT NULL REFERENCES drone (id),
    battery  NUMERIC(3) NOT NULL,
    created  TIMESTAMP  NOT NULL DEFAULT now()
);

CREATE TABLE IF NOT EXISTS medication
(
    id       INTEGER PRIMARY KEY AUTO_INCREMENT,
    name     VARCHAR NOT NULL,
    weight   INT     NOT NULL,
    code     VARCHAR NOT NULL,
    image    BLOB(5 M),
    drone_id BIGINT  NOT NULL REFERENCES drone (id)
);
