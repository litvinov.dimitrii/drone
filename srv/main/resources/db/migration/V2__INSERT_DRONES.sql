INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('367ea681a6da45988847872e7b69ac4e', 200, 'LIGHTWEIGHT', 20, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('def2d6fb653f431ea037ad7fd7df0744', 300, 'MIDDLEWEIGHT', 95, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('12bc846f82d64871a9c6d8c5d9566b03', 400, 'CRUISER_WEIGHT', 90, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('69438c9270e24591ae43c387b6628e0f', 220, 'LIGHTWEIGHT', 100, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('88413ae9baea4390ae5fc5a83f5e2831', 455, 'HEAVYWEIGHT', 85, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('b9573aa383b34cd18456c2222b356809', 333, 'MIDDLEWEIGHT', 100, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('bbf9234ae0eb4aa1875601a9ce38ff12', 400, 'CRUISER_WEIGHT', 90, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('aadccf484b974f62a4b50c8fda6e760c', 222, 'LIGHTWEIGHT', 100, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('8be6d82b33e848098374a52fd7e59f83', 490, 'HEAVYWEIGHT', 100, 'IDLE');

INSERT INTO drone(serial_number,
                  weight_limit,
                  model,
                  battery,
                  state)
VALUES ('33af1353ceba4154b98079a978d49f3c', 390, 'CRUISER_WEIGHT', 100, 'IDLE');
